function verifyInviteCode(a) {
    var b = {
        code: a
    };
    $.ajax({
        type: "POST",
        dataType: "json",
        data: b,
        url: "/api/invite/verify",
        success: function(a) {
            console.log(a);
        },
        error: function(a) {
            console.log(a);
        }
    });
}

function makeInviteCode() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/api/invite/how/to/generate",
        success: function(a) {
            console.log(a);
        },
        error: function(a) {
            console.log(a);
        }
    });
}
